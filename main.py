# Generate several results of the distributed svd for a combination of parameters
# Run without argument to see the full list

from matplotlib import rc
import matplotlib.pyplot as plt
import numpy as np
import os
import anytree as tree
import csv
import argparse

import utils
import qr

def min_max(opt):

    data_dir = "data"

    fp = open(data_dir + "/min_max.out", 'w')
    fp.write("inc\t"
             "nb_core\t"
             "matrix_name\t"
             "min_ratio\t"
             "min_ratio_qrcp\t"
             "rank\t"
             "qrtp_error\t"
             "qrcp_error\n")

    requested_k = opt.requested_rank

    inc = 0
    for matrix_path in opt.matrix_list:
        matrix_name = os.path.split(os.path.splitext(matrix_path)[0])[1]

        A = utils.read_matrix(matrix_path)
        print("\nMatrix", matrix_path, A.shape)

        _, s, _ = np.linalg.svd(A)

        for nb_cores in opt.nbs_cores:

            print("Run {} Parameters : requested_k {} nb_cores {}".format(inc, requested_k, nb_cores))

            u_approx, v_approx, error_tree = qr.split_qr_rank(A, requested_k, nb_cores, opt)
            s_approx = np.linalg.svd(u_approx @ v_approx)[1]

            # Compute full QRCP for comparison
            u_approx_qr, v_approx_qr, _, _ = qr.truncated_qr_lapack(A, requested_k)
            s_approx_qr = np.linalg.svd(u_approx_qr @ v_approx_qr)[1]

            max_ratio = np.max(np.abs(s_approx[:requested_k] / s[:requested_k]))
            min_ratio = np.min(np.abs(s_approx[:requested_k] / s[:requested_k]))
            min_ratio_qrcp = np.min(np.abs(s_approx_qr[:requested_k] / s[:requested_k]))

            qrcp_error = 0
            u_approx_qr, v_approx_qr, _, _ = qr.truncated_qr_lapack(A, requested_k)
            qrcp_error = np.linalg.norm(A - u_approx_qr @ u_approx_qr.T @ A) / np.linalg.norm(A)
            qrtp_error = np.linalg.norm(A - u_approx @ v_approx) / np.linalg.norm(A)

            fp.write(("{}\t" * 7 + "{}\n").format(
                inc,
                nb_cores,
                matrix_name,
                min_ratio,
                min_ratio_qrcp,
                requested_k,
                qrtp_error,
                qrcp_error))

            inc += 1

    fp.close()

def single_rank(opt):
    
    data_dir = "data"

    rc('text', usetex=True)
    font = {'weight': 'bold',
            'family': 'DejaVu Sans',
            'size': 15}

    rc('font', **font)

    requested_rank = opt.requested_rank
    matrix_name = utils.get_matrix_name(opt.matrix_list_single)

    A = utils.read_matrix(opt.matrix_list_single)
    print("Input matrix has size", A.shape)

    # Compute approximation
    _, s, _ = np.linalg.svd(A)

    u_approx, v_approx, error_tree = qr.split_qr_rank(A, requested_rank, opt.nbs_cores_single, opt)
    s_approx = np.linalg.svd(u_approx @ v_approx)[1]

    #utils.render_tree(error_tree)

    # Compute full QRCP for comparison
    u_approx_qr, v_approx_qr, _, _ = qr.truncated_qr_lapack(A, requested_rank)
    s_approx_qr = np.linalg.svd(u_approx_qr @ v_approx_qr)[1]

    full_error = np.linalg.norm(A - u_approx_qr @ u_approx_qr.T @ A, 2)
    print("qrcp error", full_error / np.linalg.norm(A, 2))
    bloc_error = np.linalg.norm(A - u_approx @ u_approx.T @ A, 2)
    print("qrtp error", bloc_error / np.linalg.norm(A, 2))
    print("(qrtp_error - qrcp_error) / qrcp_error =", (bloc_error - full_error) / full_error)

    # Plot
    sv_indices = np.array(range(requested_rank))
    fig = plt.figure(figsize=(10,6))
    ax = fig.add_subplot(111)
    ax.plot(sv_indices + 1, s[sv_indices], "x-", label="SVD")
    ax.plot(sv_indices + 1, s_approx_qr[sv_indices], "+-", label="QRCP")
    ax.plot(sv_indices + 1, s_approx[sv_indices], "^-", label="QRTP", fillstyle="none")
    #ax.set_xlim(left=sv_indices[0] + 1, right=sv_indices[-1] + 1)
    ax.set_xlabel(r"$i$")
    ax.legend(loc="lower left")

    ax2 = ax.twinx()
    ax2.plot(sv_indices + 1, s_approx[sv_indices] / np.abs(s[sv_indices]), "ro", label=r"$\sigma_i(A_{QRTP})/\sigma_i(A)$", alpha=0.5)
    #ax2.set_yscale("log")
    ax2.grid(True)

    ax2.set_ylabel("Error")
    ax2.legend(loc="upper right")
    ax.set_yscale("log")

    plt.savefig("pdf/" + matrix_name + "_" + str(requested_rank) + ".pdf")
    plt.show()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("matrix_list", metavar="matrix", nargs="+", help="matrix path")
    parser.add_argument("--requested-rank", "-k", dest="requested_rank", default=10, type=int)
    parser.add_argument("--cores", "-c", dest="nbs_cores", default=["4"], nargs="+", type=str, help="n for square splitting, nxm for rectangular splitting, append 'c' at then end for column then row gathering instead of nested squares gathering")
    parser.add_argument("--single-rank", dest="single_rank", action='store_true', help="single_rank")
    parser.add_argument("--disp-cols", dest="selected_cols", action='store_true', help="Display selected columns at each step")

    args = parser.parse_args()
    args.nbs_cores = [i.split(',') for i in args.nbs_cores]

    if args.single_rank:
        if len(args.matrix_list) != 1:
            raise ValueError("Expected only one matrix for single-rank")
        args.matrix_list_single = args.matrix_list[0]

        if len(args.nbs_cores) != 1:
            raise ValueError("Expected only one number or cores (nb-cores) for single-rank")
        args.nbs_cores_single = args.nbs_cores[0]

        single_rank(args)
    else:
        min_max(args)
